export interface AppointmentProps {
    //Essa interface vai anotar quais propiedades cada agendamento vai ter
    customer: string
    startsAt: Date
    endsAt: Date
}

export class Appointment {
    //Aqui dentro eu falo que tenho uma propriedade privada props do tipo AppointmentProps
    private props: AppointmentProps

    get customer() {
        return this.props.customer
    }

    get startsAt() {
        return this.props.startsAt
    }

    get endsAt() {
        return this.props.endsAt
    }

    constructor(props: AppointmentProps) {

        const { startsAt, endsAt } = props

        if (endsAt <= startsAt) {
            throw new Error('Invalid end date')
        }

        this.props = props
    }
}